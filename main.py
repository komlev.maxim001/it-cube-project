import pygame
import random
import game_objects
import menu
from config import screen_size, fps, game_size, ticks, time, tick_check, cars_position_l, cars_position_r, nums

pygame.init()
pygame.display.set_icon(pygame.image.load("Sprites/icon.bmp"))
pygame.mixer.init()
screen = pygame.display.set_mode(screen_size)
pygame.display.set_caption("Escape from robbery")
clock = pygame.time.Clock()
# pygame.display.toggle_fullscreen()
player = game_objects.Player(screen_size[0] // 2, screen_size[1] // 2, screen_size, game_size)
titles = menu.Titles(screen_size)
map = pygame.sprite.Group()
road = game_objects.Road(0, 0, screen_size)
road2 = game_objects.Road(0, 720, screen_size)
police = menu.PoliceCar(screen_size)
menu_robber = menu.RobberCar(screen_size)
game_road = pygame.sprite.Group()
start_button = menu.StartButton(screen_size)
end_button = menu.QuitButton(screen_size)
game_score = game_objects.Game_score(screen_size)
g_over_screen = game_objects.GameOver(screen_size)


surface = pygame.Surface(screen_size)

start_screen = pygame.sprite.Group()
start_screen.add(titles)
fuels = pygame.sprite.Group()
all_sprites = pygame.sprite.Group()
game_road.add(road, road2)

over_screen = pygame.sprite.Group()
over_screen.add(g_over_screen)

menu_sprites = pygame.sprite.Group()
menu_sprites.add(menu.Mainmenu(screen_size), menu.Logotype(screen_size))
menu_sprites.add(police, menu_robber, start_button, end_button)

running = True
game_over = False
score = 0
fuel = 100
n: int
difficulty = 1




def new_game():
    for i in range(2):
        global car, all_sprites, n, difficulty, score, game_over, player, fuel, all_stars
        fuels = pygame.sprite.Group()
        all_stars = pygame.sprite.Group()
        all_stars.add(game_objects.Stars(1))
        game_objects.reset_fuel()
        all_sprites = pygame.sprite.Group()
        all_sprites.add(player)
        car = game_objects.EnemyCar(random.choice(cars_position_r), random.choice(nums), screen_size, player)
        all_sprites.add(car)
        n = 1
        difficulty = 1
        game_over = False
        game_objects.game_over = False
        score = 0
        time = tick_check(ticks, -1)[1]
        fuel = 100


is_main_menu = True
is_start = True
keys = {}
new_game()
while running:
    clock.tick(fps)
    ticks = tick_check(ticks, time)[0]
    time = tick_check(ticks, time)[1]
    if is_main_menu:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                if start_button.update(event.pos):
                    is_main_menu = not is_main_menu
                if end_button.update(event.pos):
                    running = False
            else:
                if event.type == pygame.KEYDOWN:
                    keys[event.key] = True
                    if (event.key == pygame.K_RETURN or event.key == pygame.K_SPACE) and is_start == False:
                        is_main_menu = not is_main_menu
                    is_start = False
                    if event.key == pygame.K_ESCAPE:
                        running = False
                if event.type == pygame.KEYUP:
                    keys[event.key] = False
        police.update(time)
        menu_sprites.add(police)
        menu_sprites.draw(screen)
        menu_robber.update(time)
        if is_start:
            start_screen.update(ticks, screen)
            start_screen.draw(screen)
        pygame.display.flip()
    else:
        if game_over:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                if event.type == pygame.KEYDOWN:
                    keys[event.key] = True
                    if event.key == pygame.K_RETURN:
                        new_game()
                    if event.key == pygame.K_ESCAPE:
                        running = False
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    print(event.pos)
            game_objects.best_score(str(score))
            over_screen.update(screen, score)
            over_screen.draw(screen)
        else:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                if event.type == pygame.KEYDOWN:
                    keys[event.key] = True
                    if event.key == pygame.K_ESCAPE:
                        running = False
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    print(event.pos)

                if event.type == pygame.KEYUP:
                    keys[event.key] = False
            screen.fill((255, 255, 255))
            all_sprites.update(keys, all_sprites)
            game_road.update()
            game_road.draw(screen)
            all_stars.draw(screen)
            if ticks % 30 == 0:
                if time % 15 == 0 and time != 0 and ticks % 60 == 0:
                        difficulty += 1
                        all_stars.add(game_objects.Stars(difficulty))
                if difficulty < 5:
                    game_objects.spawn_car(all_sprites, player, difficulty, random.randint(0, 1))
                if time % 11 == 0 and ticks % 60 == 0:
                    game_objects.spawn_fuel(all_sprites, player)
                game_objects.fuel_update()
            all_sprites.draw(screen)
            game_score.update(time, score, screen)
            score += 1
            game_over = game_objects.check_game()
        pygame.display.flip()

pygame.quit()