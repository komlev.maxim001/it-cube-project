
import pygame
from Levels import level0 as level_0

"Глобальные настройки"
screen_size = [1280, 720]
game_size = (screen_size[0] * 2, screen_size[1] * 2)
fps = 60
ticks = 1
time = 0
cars_counter = 0

color = (211, 211, 211)
cell_size = [96, 96]
cell = 96
game_level = 0
cars_position_r = [675, 748, 828, 914, 989]
cars_position_l = [289, 444, 355, 603, 533]
cars_position_all = [675, 748, 828, 914, 989, 289, 444, 355, 603, 533]
nums = [-50, -45, -40, -35, -30, -25]

"Счетчик кадров"


def tick_check(tick, time):
    if tick > 60:
        time += 1
        return 1, time
    else:
        return tick + 1, time


