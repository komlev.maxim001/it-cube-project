import pygame


class Titles(pygame.sprite.Sprite):
    def __init__(self, screen_size):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("Sprites/bg1.jpg")
        self.rect = self.image.get_rect(center=(screen_size[0] // 2, screen_size[1] // 2))
        self.n = 1
        self.message = None
        self.screen_size = screen_size

    def blit_text(self, text, screen):
        screen.fill((10, 10, 10))
        message = pygame.font.Font('Files/Nunito-Regular.ttf', 35).render(text, True, (255, 255, 255))
        text_rect = message.get_rect(center=(self.rect.centerx, self.rect.centery))
        screen.blit(message, text_rect)

    def update(self, ticks, screen):
        if ticks < 15 or 30 < ticks < 45:
            self.image = pygame.image.load("Sprites/bg1.jpg")
        else:
            self.image = pygame.image.load("Sprites/bg2.jpg")
        self.image.set_alpha(150)
        self.image = pygame.transform.scale(self.image, self.screen_size)
        self.rect = self.image.get_rect(center=(self.screen_size[0] // 2, self.screen_size[1] // 2))
        if self.n <= 25:
            if ticks % 3 == 0:
                f = open('text.txt', 'r')
                self.message = f.read(self.n)
                self.n += 1
        self.blit_text(self.message, screen)


class Mainmenu(pygame.sprite.Sprite):
    def __init__(self, screen_size):
        pygame.sprite.Sprite.__init__(self)
        self.screen_size = screen_size
        self.image = pygame.image.load("Sprites/bg.jpg")
        self.image = pygame.transform.scale(self.image, (self.screen_size))
        self.rect = self.image.get_rect(center=(screen_size[0] // 2, screen_size[1] // 2))
        self.rect.center = self.screen_size[0] // 2, self.screen_size[1] // 2


class PoliceCar(pygame.sprite.Sprite):
    def __init__(self, screen_size):
        pygame.sprite.Sprite.__init__(self)
        self.screen_size = screen_size
        self.image = pygame.image.load("Sprites/police.png")
        self.image = pygame.transform.scale(self.image, (int(self.image.get_rect().w * 2.66), int(self.image.get_rect().h * 2.66) ))
        self.rect = self.image.get_rect(center=(screen_size[0] + 150, screen_size[1] // 2))

    def update(self, ticks):
        if self.rect.centerx < -100:
            self.rect = self.image.get_rect(center=(self.screen_size[0] + 100, self.screen_size[1] // 2))
        self.rect.x -= 6


class RobberCar(pygame.sprite.Sprite):
    def __init__(self, screen_size):
        pygame.sprite.Sprite.__init__(self)
        self.screen_size = screen_size
        self.image = pygame.image.load("Sprites/menu_robber.png")
        self.image = pygame.transform.scale(self.image, (int(self.image.get_rect().w * 2.66), int(self.image.get_rect().h * 2.66) ))
        self.rect = self.image.get_rect(center=(screen_size[0], (screen_size[1] // 2) + 25))

    def update(self, ticks):
        if self.rect.centerx < -100:
            self.rect = self.image.get_rect(center=(self.screen_size[0] + 100, self.screen_size[1] // 2 + 25))
        self.rect.x -= 6


class Logotype(pygame.sprite.Sprite):
    def __init__(self, screen_size):
        pygame.sprite.Sprite.__init__(self)
        self.screen_size = screen_size
        self.image = pygame.image.load("Sprites/logo.png")
        self.rect = self.image.get_rect(center=(screen_size[0] // 2, 150))


class StartButton(pygame.sprite.Sprite):
    def __init__(self, screen_size):
        pygame.sprite.Sprite.__init__(self)
        self.screen_size = screen_size
        self.image = pygame.image.load("Sprites/start.png")
        self.rect = self.image.get_rect(center=(screen_size[0] // 2,  550))

    def update(self, mouse_pos):
        return self.rect.collidepoint(mouse_pos[0], mouse_pos[1])

class QuitButton(pygame.sprite.Sprite):
    def __init__(self, screen_size):
        pygame.sprite.Sprite.__init__(self)
        self.screen_size = screen_size
        self.image = pygame.image.load("Sprites/guit.png")
        self.rect = self.image.get_rect(center=(screen_size[0] // 2, 650))

    def update(self, mouse_pos):
        return self.rect.collidepoint(mouse_pos[0], mouse_pos[1])

