import pygame
import random
from config import cars_position_r, cars_position_l, screen_size, nums, color, cars_position_all

f = open('best-score.txt', 'r')
best = int(f.read())
f.close()

game_over = False
is_best = False
difficulty = 0
fuel = 100
cars_counter = 2
class Player(pygame.sprite.Sprite):
    def __init__(self, pos_x, pos_y, screen_s, game_size):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("Sprites/robber.png")
        self.image = pygame.transform.scale(self.image,
                                            (int(self.image.get_rect().w * 2.66), int(self.image.get_rect().h * 2.66)))
        self.screen_size = screen_s
        self.game_size = game_size
        self.rect = self.image.get_rect()
        self.velocity = [0, 0]
        self.boost = [0, 0]
        self.boost = [0, 0.0]
        self.position = self.rect
        self.position = pygame.Rect(pos_x, pos_y, self.rect.w, self.rect.h)
        self.rect.center = (screen_size[0] / 2, screen_size[1] - 96 - 10)

    def update(self, keys, all_sprites):
        global fuel, game_over
        if fuel <= 0:
            game_over = True
        self.rect.x = self.position.x
        self.rect.y = self.position.y

        if keys.get(pygame.K_UP):
            self.position[1] -= 3
        if keys.get(pygame.K_LEFT, False):
            self.position[0] -= 5
            self.position[1] += 1
        if keys.get(pygame.K_RIGHT, False):
            self.position[0] += 5
            self.position[1] += 1
        if keys.get(pygame.K_DOWN):
            self.position[1] += 5
        self.position.y = max(self.position[1], 0)
        self.position.y = min(self.position[1], screen_size[1] - 40)
        self.position.x = max(self.position[0], 255)
        self.position.x = min(self.position[0], screen_size[0] - 300)



class Road(pygame.sprite.Sprite):
    def __init__(self, pos_x, pos_y, screen_s):
        pygame.sprite.Sprite.__init__(self)
        self.screen_size = screen_s
        self.velocity = 6
        self.pos_x = pos_x
        self.image = pygame.image.load("Sprites/road.jpg")
        self.image = pygame.transform.scale(self.image, screen_s)
        self.rect = self.image.get_rect(center=(screen_size[0] // 2, pos_y))

    def update(self):
        self.rect.y += self.velocity
        if self.rect.top > self.screen_size[1]:
            self.rect.y = -720 + self.velocity


class EnemyCar(pygame.sprite.Sprite):
    def __init__(self, pos_x, pos_y, screen_s, player):
        pygame.sprite.Sprite.__init__(self)
        self.screen_size = screen_s
        self.velocity = 4
        self.side = 1
        self.player = player
        self.image = pygame.image.load("Sprites/car2.png")
        self.image = pygame.transform.scale(self.image,
                                            (int(self.image.get_rect().w * 2.66), int(self.image.get_rect().h * 2.66)))
        self.rect = self.image.get_rect(center=(pos_x, pos_y))

    def update(self, keys, cars):
        global cars_counter, difficulty, game_over
        self.rect.y += self.velocity

        if self.rect.centery > self.screen_size[1] + 50:
            self.kill()
            cars_counter -= 1
            spawn_car(cars, self.player, difficulty, self.side)
        if pygame.sprite.collide_mask(self, self.player):
            game_over = True



class EnemyCar_l(pygame.sprite.Sprite):
    def __init__(self, pos_x, screen_s, pos_y, player):
        pygame.sprite.Sprite.__init__(self)
        self.screen_size = screen_s
        self.velocity = 10
        self.player = player
        self.side = 0
        self.image = pygame.image.load("Sprites/car1.png")
        self.image = pygame.transform.scale(self.image,
                                            (int(self.image.get_rect().w * 2.66), int(self.image.get_rect().h * 2.66)))
        self.rect = self.image.get_rect(center=(pos_x, pos_y))

    def update(self, keys, cars):
        global cars_counter, difficulty, game_over
        self.rect.y += self.velocity

        if self.rect.centery > self.screen_size[1] + 50:
            self.kill()
            cars_counter -= 1
            spawn_car(cars, self.player, difficulty, self.side)
        if pygame.sprite.collide_mask(self, self.player):
            game_over = True

class Block(pygame.sprite.Sprite):
    def __init__(self, pos_x, pos_y, screen_s):
        pygame.sprite.Sprite.__init__(self)
        self.screen_size = screen_s
        self.velocity = 10
        self.side = 0
        self.image = pygame.image.load("Sprites/car1.png")
        self.image = pygame.transform.scale(self.image,
                                            (int(self.image.get_rect().w * 2.66), int(self.image.get_rect().h * 2.66)))
        self.rect = self.image.get_rect(center=(pos_x, pos_y))

    def destroy(self):
        self.kill()

def spawn_car(cars, player, dif, side):
    global cars_counter, screen_size, cars_position_l, cars_position_r, difficulty, nums
    difficulty = dif
    x = 5 * difficulty
    if cars_counter <= x:
        is_accept = False
        if side == 1:
            while not is_accept:
                car_pos = random.choice(cars_position_r)
                pos_y = random.choice(nums)
                block = Block(car_pos, pos_y, screen_size)
                if not pygame.sprite.spritecollideany(block, cars):
                    block.destroy()
                    is_accept = not is_accept
            cars.add(EnemyCar(car_pos, pos_y, screen_size, player))
        else:
            while not is_accept:
                car_pos = random.choice(cars_position_l)
                pos_y = random.choice(nums)
                block = Block(car_pos, pos_y, screen_size)
                if not pygame.sprite.spritecollideany(block, cars):
                    block.destroy()
                    is_accept = not is_accept
            cars.add(EnemyCar_l(car_pos, screen_size, pos_y, player))
        cars_counter += 1



class Stars(pygame.sprite.Sprite):
    def __init__(self, pos_x):
        pygame.sprite.Sprite.__init__(self)
        self.velocity = 4
        self.image = pygame.image.load("Sprites/star.png")
        self.image = pygame.transform.scale(self.image,
                                            (int(self.image.get_rect().w * 1.4), int(self.image.get_rect().h * 1.4)))
        self.rect = self.image.get_rect(center=(1020 + (50 * pos_x), 240))


class Game_score(pygame.sprite.Sprite):
    def __init__(self, screen_size):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface(screen_size)
        self.message = None
        self.screen_size = screen_size
        self.rect = self.image.get_rect()

    def blit_text(self, text, screen, position):
        global color
        bar = pygame.image.load("Sprites/bar.png")
        message = pygame.font.Font('Files/Game-Font.ttf', 30).render(text, True, (color))
        text_rect = message.get_rect(center=(1150, position))
        screen.blit(bar, bar.get_rect(center=(1160, position)))
        screen.blit(message, text_rect)

    def update(self, time, score, screen):
        global best, is_best, fuel
        if score < best:
            is_best = True
            self.blit_text(" Best: " + str(best), screen, 90)
        else:
            self.blit_text("Best: " + str(score), screen, 90)
        self.blit_text("Score: " + str(score), screen, 40)
        self.blit_text("Time: " + str(time), screen, 190)
        self.blit_text("Fuel: " + str(fuel) + "%", screen, 140)


class GameOver(pygame.sprite.Sprite):
    def __init__(self, screen_size):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface(screen_size)
        self.image.fill((0, 0, 0))
        self.image.set_alpha(20)
        self.message = None
        self.tick = 0
        self.screen_size = screen_size
        self.rect = self.image.get_rect()

    def blit_text(self, text, size, screen, position):
        message = pygame.font.Font('Files/Game-Font.ttf', size).render(text, True, (255, 255, 255))
        text_rect = message.get_rect(center=(screen_size[0] // 2, position))
        screen.blit(message, text_rect)

    def update(self, screen, score):
        global is_best
        self.tick += 1
        if self.tick > 50:
            self.blit_text("Game over", 100, screen, 150)
        if self.tick > 100:
            self.blit_text("Your score is " + str(score), 40, screen, 300)
        if self.tick > 150:
            self.blit_text("To play new game press [enter]", 40, screen, 500)
            self.blit_text("To quit game press [esc]", 40, screen, 600)

class GameFuel(pygame.sprite.Sprite):
    def __init__(self, pos_x, pos_y, screen_size, player):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("Sprites/fuel.png")
        self.player = player
        self.velocity = 4
        self.screen_size = screen_size
        self.rect = self.image.get_rect()
        self.image = pygame.transform.scale(self.image,
                                            (int(self.image.get_rect().w * 2), int(self.image.get_rect().h * 2)))
        self.rect = self.image.get_rect(center=(pos_x, pos_y))

    def update(self, keys, cars):
        global fuel
        self.rect.y += self.velocity
        if pygame.sprite.collide_mask(self, self.player):
            fuel += 30
            fuel = min(fuel, 100)
            self.kill()


def spawn_fuel(sprites,player):
    is_accept = False
    global screen_size, cars_position_all, nums
    while not is_accept:
        fuel_pos = random.choice(cars_position_r)
        pos_y = random.choice(nums)
        block = Block(fuel_pos, pos_y, screen_size)
        if not pygame.sprite.spritecollideany(block, sprites):
            block.destroy()
            is_accept = not is_accept
    sprites.add(GameFuel(fuel_pos, pos_y, screen_size, player))


def check_game():
    global game_over, cars_counter
    if game_over:
        cars_counter = 1
        return True
    else:
        return False


def reset_fuel():
    global fuel
    fuel = 100


def fuel_update():
    global fuel
    fuel -= 1


def best_score(score):
    global is_best
    if is_best:
        s = open('best-score.txt', 'w')
        s.write(score)
        s.close()